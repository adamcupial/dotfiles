# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh
export ANDROID_HOME="/Users/adamcupial/Library/Android/sdk"
export NPM_PACKAGES="/Users/adamcupial/.npm-packages"
export NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
export GRUNT_TARGET="dev"
export WORKON_HOME=$HOME/.virtualenvs
export PROJECT_HOME=$HOME/private-projects
export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/opt/X11/lib/pkgconfig
export PATH=/Users/adamcupial/.gem/ruby/2.3.0/bin:/Users/adamcupial/.gem/ruby/bin:/Users/adamcupial/projects/woff2:/usr/local/bin:/usr/local/sbin:/Users/adamcupial/.npm-packages/bin:$PATH
export TERM="xterm-256color"
# export PYTHONPATH=`brew --prefix`/lib/python2.7/site-packages:$PYTHONPATH
source /usr/local/bin/virtualenvwrapper.sh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
# ZSH_THEME="robbyrussell"
# ZSH_THEME="agnoster"
ZSH_THEME="powerlevel9k/powerlevel9k"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    brew
    node
    npm
    osx
    pip
    python
    terminalapp
    colorize
    colored-man
    cp
    extract
    virtualenvwrapper
    fzf-zsh
    docker-compose
    zsh-syntax-highlighting
    zsh-autosuggestions
    ssh-agent
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
vm() {cd ~/.dotvm; vagrant $*; cd -;}
vmp() {vm ssh -c "project $*" zerobox7;}

export PROXY_MGMT_USER="goal"
export PROXY_MGMT_TOKEN="8cf14vtzpm6poxxp8ctxodvjwpjlxt"
qa_auth() {qa-endpoint --domain $1.goal-website.hhdadm-p0web01.dev.performgroup.com ${*:2};}
qa_auth2() {qa-endpoint --domain $1.goal-website.hhdadm-p0web02.dev.performgroup.com ${*:2};}

alias terncondense="/Users/adamcupial/.vim/plugged/tern_for_vim/node_modules/tern/bin/condense"
alias gdo="git diff origin/`git rev-parse --abbrev-ref HEAD`"
alias gpf="git push --force-with-lease"
alias word='sed `perl -e "print int rand(99999)"`"q;d" /usr/share/dict/words'

DEFAULT_USER=adamcupial

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
export PATH="/usr/local/opt/gettext/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/gettext/lib"
export CPPFLAGS="-I/usr/local/opt/gettext/include"

zstyle :omz:plugins:ssh-agent agent-forwarding on

