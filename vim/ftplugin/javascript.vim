set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set autoindent
set foldmethod=syntax
set foldlevel=3
" set foldmarker={,}
:syntax on
