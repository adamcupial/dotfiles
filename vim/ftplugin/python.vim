set tabstop=4
set expandtab
set smarttab
set shiftwidth=4
set softtabstop=4
set autoindent
set foldmethod=syntax
set cinwords=if,elif,else,for,while,try,except,finally,def,class,with
set relativenumber

autocmd BufRead *.py set smartindent cinwords=if,elif,else,for,while,try,except,finally,def,class
autocmd BufWritePre *.py normal m`:%s/\s\+$//e``
